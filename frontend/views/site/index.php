<?php
/* @var $this yii\web\View */
$this->title = Yii::$app->name;
?>
<div class="site-index">

    <?php echo \common\widgets\DbCarousel::widget([
        'key'=>'carousel',
        'options' => [
            'class' => 'slide', // enables slide effect
        ],
    ]) ?>
    
</div>
