<?php
use frontend\assets\FrontendAsset;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this \yii\web\View */
/* @var $content string */

$bundle = FrontendAsset::register($this);

$this->beginContent('@frontend/views/layouts/_clear.php')
?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<span class="logo-lg"><b>IER </b>School</span>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]); ?>
    <?php echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('frontend', 'Home'), 'url' => ['/site/index']],
            ['label' => Yii::t('frontend', 'Grades'), 'url' => ['/grades/index'], 'visible'=>!Yii::$app->user->isGuest],
            [
                'label' => Yii::t('frontend', 'Backend'),
                'url' => Yii::getAlias('@backendUrl'),
                'visible'=>Yii::$app->user->can('manager')
            ],
            ['label' => Yii::t('frontend', 'Login'), 'url' => ['/user/sign-in/login'], 'visible'=>Yii::$app->user->isGuest],
            [
                'label' => Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->getPublicIdentity(),
                'visible'=>!Yii::$app->user->isGuest,
                'items'=>[
                    [
                        'label' => Yii::t('frontend', 'Settings'),
                        'url' => ['/user/default/index']
                    ],
                    [
                        'label' => Yii::t('frontend', 'Logout'),
                        'url' => ['/user/sign-in/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ]
                ]
            ],
            [
                'label'=>Yii::t('frontend', 'Language'),
                'items'=>array_map(function ($code) {
                    return [
                        'label' => Yii::$app->params['availableLocales'][$code],
                        'url' => ['/site/set-locale', 'locale'=>$code],
                        'active' => Yii::$app->language === $code
                    ];
                }, array_keys(Yii::$app->params['availableLocales'])),
                'visible'=>Yii::$app->user->isGuest
            ]
        ]
    ]); ?>
    <?php NavBar::end(); ?>

    <?php echo $content ?>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"><strong>Copyright &copy;  <?php echo date('Y') ?> <a>IER School</a>.</strong>
            <?php  echo Yii::t('backend', 'All rights reserved').'.'; ?></p>
        <p class="pull-right">
            <b> <?php   echo \Yii::t('frontend', 'Development by'); ?> </b>
            <?php
            echo ' <a>'. Yii::$app->keyStorage->get('app-developer', 'José Peña') .'</a>. ';
            echo Yii::t('backend', 'Version');
            echo ' '.Yii::$app->keyStorage->get('app-version', '1.0.0');
            ?>
        </p>
</footer>
<?php $this->endContent() ?>