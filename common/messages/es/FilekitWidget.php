<?php
/**
 * Project yii2-file-kit.
 * Author: Eugene Terentev <eugene@terentev.net>
 */
return [
    'Maximum number of files exceeded' => 'Se ha alcanzado el número máximo de archivos',
    'File type not allowed' => 'Tipo de archivo no permitido',
    'File is too large' => 'El archivo es demasiado grande',
    'File is too small' => 'El archivo es demasiado pequeño'
];