<?php

namespace common\components;

use webvimark\modules\UserManagement\components\UserConfig as BaseUserConfig;

class UserConfig extends BaseUserConfig
{
	/**
	 * @inheritdoc
	 */
	public $identityClass = 'backend\models\User';
}
