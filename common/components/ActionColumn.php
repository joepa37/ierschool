<?php

namespace common\components;

use yii\grid\ActionColumn as BaseActionColumn;
use Yii;
use webvimark\modules\UserManagement\components\GhostHtml;

class ActionColumn  extends BaseActionColumn
{
    public $ajaxButtons = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->initDefaultButtons();
        $this->setColumnWidth();
    }

    protected function setColumnWidth()
    {
        if (!isset($this->contentOptions['style'])) {
            $width = 28;
            $count = preg_match_all('/\\{([\w\-\/]+)\\}/',$this->template);
            $this->contentOptions['style'] = 'width:'.($width * $count).'px; text-align:center;';
        }
    }

    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'View'),
                    'aria-label' => Yii::t('yii', 'View'),
                    'data-pjax' => '0',
                ], $this->buttonOptions);
                if (in_array('view', $this->ajaxButtons)) {
                    $options = array_merge( $options, [
                        'class' => 'showModalForm '.(isset($options['class']) ? $options['class'] : ''),
                        'value' => $url,
                    ]);
                    return GhostHtml::a('<span class="glyphicon glyphicon-eye-open"></span>', false, $options);
                }
                return GhostHtml::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Update'),
                    'aria-label' => Yii::t('yii', 'Update'),
                    'data-pjax' => '0',
                    'class' => 'showModalForm',
                    'value' => $url,
                ], $this->buttonOptions);
                if (in_array('update', $this->ajaxButtons)) {
                    $options = array_merge( $options, [
                        'class' => 'showModalForm '.(isset($options['class']) ? $options['class'] : ''),
                        'value' => $url,
                    ]);
                    return GhostHtml::a('<span class="glyphicon glyphicon-pencil"></span>', false, $options);
                }
                return GhostHtml::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Delete'),
                    'aria-label' => Yii::t('yii', 'Delete'),
                    'data-pjax' => '0',
                ], $this->buttonOptions);
                if (in_array('delete', $this->ajaxButtons)) {
                    $options = array_merge( $options, [
                        'class' => 'showModalDelete '.(isset($options['class']) ? $options['class'] : ''),
                        'data-action' => $url,
                    ]);
                    return GhostHtml::a('<span class="glyphicon glyphicon-trash"></span>', false, $options);
                }
                $options['data-confirm'] = Yii::t('yii', 'Are you sure you want to delete this item?');
                $options['data-method'] = 'post';
                return GhostHtml::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['confirm'])) {
            $this->buttons['confirm'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => 'Confirmar',
                    'aria-label' => 'Confirmar',
                    'data-pjax' => '0',
                ], $this->buttonOptions);
                return GhostHtml::a('<span class="glyphicon glyphicon-check"></span>', $url, $options);
            };
        }
    }
}