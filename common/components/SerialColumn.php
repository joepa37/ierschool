<?php

namespace common\components;
use yii\grid\SerialColumn as BaseSerialColumn;

class SerialColumn extends BaseSerialColumn
{
    public $options = ['style'=>'width:45px'];
}