<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_activity_log".
 *
 * @property integer $user_id
 * @property string $ipaddress
 * @property string $logtime
 * @property string $controller
 * @property string $action
 * @property string $param1
 * @property string $param2
 * @property string $param3
 * @property string $param4
 * @property string $extra_params
 * @property string $os
 * @property string $browser
 * @property string $browser_version
 * @property string $user_agent
 */
class UserActivityLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_activity_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ipaddress', 'controller', 'action', 'os', 'browser', 'browser_version', 'user_agent'], 'required'],
            [['user_id'], 'integer'],
            [['logtime'], 'safe'],
            [['extra_params'], 'string'],
            [['ipaddress', 'controller'], 'string', 'max' => 50],
            [['action', 'browser'], 'string', 'max' => 30],
            [['param1', 'param2', 'param3', 'param4'], 'string', 'max' => 255],
            [['os'], 'string', 'max' => 20],
            [['browser_version'], 'string', 'max' => 10],
            [['user_agent'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'ipaddress' => 'IP',
            'logtime' => 'Hora de Visita',
            'controller' => 'Controlador',
            'action' => 'Acción',
            'param1' => 'Parámetro 1',
            'param2' => 'Parámetro 2',
            'param3' => 'Parámetro 3',
            'param4' => 'Parámetro 4',
            'extra_params' => 'Parámetros adicionales',
            'os' => 'SO',
            'browser' => 'Navegador',
            'browser_version' => 'Versión del Navegador',
            'user_agent' => 'Agente del usuario',
        ];
    }
    
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
