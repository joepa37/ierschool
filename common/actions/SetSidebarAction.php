<?php
/**
 * Author: José Peña <joeap37@gmail.com>
 */

namespace common\actions;

use yii\base\Action;
use yii\base\InvalidParamException;
use Yii;
use yii\web\Cookie;


class SetSidebarAction extends Action
{

    /**
     * @var string
     */
    public $_sidebarCookieName = '_sidebar';

    /**
     * @var integer
     */
    public $cookieExpire;

    /**
     * @var string
     */
    public $cookieDomain;

    /**
     * @var \Closure
     */
    public $callback;


    /**
     * @param $sidebarCookieName
     * @return mixed|static
     */
    public function run($sidebarCookieName)
    {
        $cookie = new Cookie([
            'name' => $this->_sidebarCollapseCookieName,
            'value' => $sidebarCookieName,
            'expire' => $this->cookieExpire ?: time() + 60 * 60 * 24 * 365,
            'domain' => $this->cookieDomain ?: '',
        ]);
        Yii::$app->getResponse()->getCookies()->add($cookie);
        if ($this->callback && $this->callback instanceof \Closure) {
            return call_user_func_array($this->callback, [
                $this,
                $sidebarCookieName
            ]);
        }
        return Yii::$app->response->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }
}
