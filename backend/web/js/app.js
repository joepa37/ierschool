$(function() {
    "use strict";

    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();
    $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");
});

$(document).on('ready pjax:success', function() {

    //funciona para create/view/update desde el index
    /*
     * Muestra un modal al hacer click en un elemento con la clase {.modalLink}
     * Obtiene el contenido de una peticion GET, la ruta se obtiene del atributo {value} del elemento
     * El resultado se inserta en {#modal .modal-body}
     * Si ya existe la etiqueta h2
     */

    //@todo, Traducciones de los textos

    $('.showModalForm').click(function(){
        modalCrud( $(this) );
    });

    $('.showModalDelete').click(function(){
        deleteEntry( $(this) );
    });

    function modalCrud(btn)
    {
        $('#modalForm .modal-body')
            .load(btn.attr('value'), function(){

                    var $title = $('#modalForm .modal-body h2');
                    if($('#modalForm .modal-header h4').length){
                        $('#modalForm .modal-header h4').html($title.text());
                    }else{
                        $('#modalForm .modal-header').append('<h4>' + $title.text()  +'</h4>');
                    }
                    $title.remove();

                    $('#modalForm').modal('show');

                    modalAjaxForm();

                    $('.showModalDelete').click(function(){
                        deleteEntry( $(this) );
                    });

                    $('#update-item').click(function(){
                        //$('#modal').modal('hide');
                        modalCrud( $(this) );
                    });
                }
            );
    }

    function modalAjaxForm()
    {
        $('div.ajaxForm form.item-form')
            .on('beforeSubmit', function(e){
                var $form = $(this);
                $.post(
                    $form.attr('action'),
                    $form.serialize()
                    )
                    .done(function(result) {
                        if(result.status == 200){
                            //@todo, sino se declaro el pjax #items-grid, redirect a index
                            $.pjax.reload({container: '#items-grid'});
                            $(document).find('#modalForm').modal('hide');
                        }else{
                            $(document).find('#modalForm .modal-body').html(result);
                            $('#modalForm .modal-body h2').remove();
                            $('#modalForm .modal-body h4').remove();
                            modalAjaxForm();
                        }
                    })
                    .fail(function(){
                        console.log('Error 500');
                    });
                return false;
            })
            .on('submit', function(e){
                e.preventDefault();
            });
    }

    function deleteEntry(btn)
    {
        var response = confirm('¿Esta seguro de eliminar este registro?')
        if(response) {
            $.post(
                btn.attr('data-action')
                )
                .done(function(result) {
                    if(result.status == 200){
                        $.pjax.reload({container: '#items-grid'});
                        $(document).find('#modalForm').modal('hide');
                    }else{
                        alert(result.message)
                    }
                })
                .fail(function(){
                    console.log('server error');
                });
        }
        return false;
    }
});