<?php

namespace backend\controllers\UserManagement;

use webvimark\modules\UserManagement\controllers\UserPermissionController as BaseUserPermissionController;
use webvimark\modules\UserManagement\models\rbacDB\Permission;
use webvimark\modules\UserManagement\models\User;
use yii\web\NotFoundHttpException;
use Yii;

class UserPermissionController extends BaseUserPermissionController
{
    use \common\components\log\BaseTraitController;


    public function actionSet($id)
    {
        $this->logParam1 = $id;
        $user = User::findOne($id);

        if ( !$user )
        {
            throw new NotFoundHttpException('User not found');
        }

        $permissionsByGroup = [];
        $permissions = Permission::find()
            ->andWhere([
                Yii::$app->getModule('user-management')->auth_item_table . '.name'=>array_keys(Permission::getUserPermissions($user->id))
            ])
            ->joinWith('group')
            ->all();

        foreach ($permissions as $permission)
        {
            $permissionsByGroup[@$permission->group->name][] = $permission;
        }

        return $this->renderIsAjax('@backend/views/UserManagement/user-permission/set', compact('user', 'permissionsByGroup'));
    }

    public function actionSetRoles($id)
    {
        $this->logParam1 = $id;
        $this->logParam2 = implode(';', Yii::$app->request->post('roles', []));
        return parent::actionSetRoles($id);
    }
}
