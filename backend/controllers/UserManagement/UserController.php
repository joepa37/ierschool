<?php

namespace backend\controllers\UserManagement;

use common\models\UserProfile;
use webvimark\modules\UserManagement\controllers\UserController as BaseUserController;
use Yii;
use backend\models\User;
use webvimark\modules\UserManagement\models\search\UserSearch;

class UserController extends BaseUserController
{
    use \common\components\log\BaseTraitController;

    /**
     * @var User
     */
    public $modelClass = 'backend\models\User';

    /**
     * @var UserSearch
     */
    public $modelSearchClass = 'backend\models\search\UserSearch';

    public function actionIndex()
    {
        $searchModel  = $this->modelSearchClass ? new $this->modelSearchClass : null;

        if ( $searchModel )
        {
                $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        }
        else
        {
                $modelClass = $this->modelClass;
                $dataProvider = new ActiveDataProvider([
                        'query' => $modelClass::find(),
                ]);
        }

        return $this->renderIsAjax('@backend/views/UserManagement/user/index', compact('dataProvider', 'searchModel'));
    }

    public function actionView($id)
    {
        $this->logParam1 = $id;
        return $this->renderIsAjax('@backend/views/UserManagement/user/view', [
                'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new User(['scenario'=>'newUser']);

        if ( $model->load(Yii::$app->request->post()) && $model->save() )
        {
            return $this->redirect(['view',	'id' => $model->id]);
        }

        return $this->renderIsAjax('@backend/views/UserManagement/user/create', compact('model'));
    }

    public function actionUpdate($id)
    {
        $this->logParam1 = $id;
        $model = $this->findModel($id);

        if ( $this->scenarioOnUpdate )
        {
            $model->scenario = $this->scenarioOnUpdate;
        }

        if ( $model->load(Yii::$app->request->post()) AND $model->save())
        {
            $redirect = $this->getRedirectPage('update', $model);

            return $redirect === false ? '' : $this->redirect($redirect);
        }

        return $this->renderIsAjax('@backend/views/UserManagement/user/update', compact('model'));
    }
    
    public function actionDelete($id)
    {
        $this->logParam1 = $id;
        return parent::actionDelete($id);
    }
    
    public function actionBulkActivate($attribute = 'active')
    {
        $this->logParam1 = implode(';', Yii::$app->request->post('selection', []));
        $this->logParam2 = $attribute;
        parent::actionBulkActivate($attribute);
    }
    
    public function actionBulkDelete()
    {
        $this->logParam1 = implode(';', Yii::$app->request->post('selection', []));
        parent::actionBulkDelete();
    }
    
    public function actionBulkDeactivate($attribute = 'active')
    {
        $this->logParam1 = implode(';', Yii::$app->request->post('selection', []));
        $this->logParam2 = $attribute;
        parent::actionBulkDeactivate($attribute);
    }

    public function actionChangePassword($id)
    {
        $model = User::findOne($id);

        if ( !$model )
        {
            throw new NotFoundHttpException('User not found');
        }

        $model->scenario = 'changePassword';

        if ( $model->load(Yii::$app->request->post()) && $model->save() )
        {
            return $this->redirect(['view',	'id' => $model->id]);
        }

        return $this->renderIsAjax('@backend/views/UserManagement/user/changePassword', compact('model'));
    }

    protected function sendCreationNotificationEmail($user, $pwd='')
    {
        try {
            return Yii::$app->mailer->compose('@backend/mail/UserManagement/CreationNotificationMail', ['user' => $user, 'pwd' => $pwd])
                      ->setFrom(Yii::$app->getModule('user-management')->mailerOptions['from'])
                      ->setTo($user->email)
                      ->setSubject('Usuario ' . Yii::$app->name)
                      ->send();   
        } catch (\Swift_TransportException $e){
            return false;
        }
    }
}
