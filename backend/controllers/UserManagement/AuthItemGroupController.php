<?php

namespace backend\controllers\UserManagement;

use webvimark\modules\UserManagement\controllers\AuthItemGroupController as BaseAuthItemGroupController;
use yii\web\Response;
use Yii;

class AuthItemGroupController extends BaseAuthItemGroupController
{
    use \common\components\log\BaseTraitController;

    public function actionIndex()
    {
        $searchModel  = $this->modelSearchClass ? new $this->modelSearchClass : null;

        if ( $searchModel )
        {
            $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        }
        else
        {
            $modelClass = $this->modelClass;
            $dataProvider = new ActiveDataProvider([
                'query' => $modelClass::find(),
            ]);
        }
        return $this->renderIsAjax('@backend/views/UserManagement/auth-item-group/index', compact('dataProvider', 'searchModel'));
    }

    public function actionView($id)
    {
        $this->logParam1 = $id;
        return $this->renderIsAjax('@backend/views/UserManagement/auth-item-group/view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new $this->modelClass;

        if ( $this->scenarioOnCreate )
        {
            $model->scenario = $this->scenarioOnCreate;
        }

        if ( $model->load(Yii::$app->request->post()) && $model->save() )
        {
            $redirect = $this->getRedirectPage('create', $model);

            return $redirect === false ? '' : $this->redirect($redirect);
        }

        return $this->renderIsAjax('@backend/views/UserManagement/auth-item-group/create', compact('model'));
    }

    public function actionUpdate($id)
    {
        $this->logParam1 = $id;
        $model = $this->findModel($id);

        if ( $this->scenarioOnUpdate )
        {
            $model->scenario = $this->scenarioOnUpdate;
        }

        if ( $model->load(Yii::$app->request->post()) AND $model->save())
        {
            if(Yii::$app->request->isAjax)
            {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['status' => 200];
            }

            $redirect = $this->getRedirectPage('update', $model);

            return $redirect === false ? '' : $this->redirect($redirect);
        }

        return $this->renderIsAjax('@backend/views/UserManagement/auth-item-group/update', compact('model'));
    }

    public function actionDelete($id)
    {
        $this->logParam1 = $id;
        parent::actionDelete($id);
    }
}
