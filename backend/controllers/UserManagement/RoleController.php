<?php

namespace backend\controllers\UserManagement;

use webvimark\modules\UserManagement\controllers\RoleController as BaseRoleController;
use webvimark\modules\UserManagement\models\rbacDB\Role;
use yii\web\Response;
use webvimark\modules\UserManagement\components\AuthHelper;
use webvimark\modules\UserManagement\models\rbacDB\Permission;
use yii\rbac\DbManager;
use Yii;

class RoleController extends BaseRoleController
{
    use \common\components\log\BaseTraitController;

    public function actionIndex()
    {
        $searchModel  = $this->modelSearchClass ? new $this->modelSearchClass : null;

        if ( $searchModel )
        {
            $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        }
        else
        {
            $modelClass = $this->modelClass;
            $dataProvider = new ActiveDataProvider([
                'query' => $modelClass::find(),
            ]);
        }

        return $this->renderIsAjax('@backend/views/UserManagement/role/index', compact('dataProvider', 'searchModel'));
    }

    public function actionView($id)
    {
        $this->logParam1 = $id;
        $role = $this->findModel($id);

        $authManager = Yii::$app->authManager instanceof DbManager ? Yii::$app->authManager : new DbManager();

        $allRoles = Role::find()
            ->asArray()
            ->andWhere('name != :current_name', [':current_name'=>$id])
            ->all();

        $permissions = Permission::find()
            ->andWhere(Yii::$app->getModule('user-management')->auth_item_table . '.name != :commonPermissionName', [':commonPermissionName'=>Yii::$app->getModule('user-management')->commonPermissionName])
            ->joinWith('group')
            ->all();

        $permissionsByGroup = [];
        foreach ($permissions as $permission)
        {
            $permissionsByGroup[@$permission->group->name][] = $permission;
        }

        $childRoles = $authManager->getChildren($role->name);

        $currentRoutesAndPermissions = AuthHelper::separateRoutesAndPermissions($authManager->getPermissionsByRole($role->name));

        $currentPermissions = $currentRoutesAndPermissions->permissions;

        return $this->renderIsAjax('@backend/views/UserManagement/role/view', compact('role', 'allRoles', 'childRoles', 'currentPermissions', 'permissionsByGroup'));
    }
    
    public function actionSetChildRoles($id)
    {
        $this->logParam1 = $id;
        $this->logParam2 = implode(';', Yii::$app->request->post('child_roles', []));
        return parent::actionSetChildRoles($id);
    }
    
    public function actionSetChildPermissions($id)
    {
        $this->logParam1 = $id;
        $this->logParam2 = implode(';', Yii::$app->request->post('child_permissions', []));
        return parent::actionSetChildPermissions($id);
    }
    
    public function actionCreate()
    {
        $model = new Role;
        $model->scenario = 'webInput';

        if ( $model->load(Yii::$app->request->post()) && $model->save() )
        {
            $this->logParam1 = $model->name;
            return $this->redirect(['view', 'id'=>$model->name]);
        }

        return $this->renderIsAjax('@backend/views/UserManagement/role/create', compact('model'));
    }
    
    public function actionUpdate($id)
    {
        $this->logParam1 = $id;

        $model = $this->findModel($id);
        $model->scenario = 'webInput';

        if ( $model->load(Yii::$app->request->post()) AND $model->save())
        {
            if(Yii::$app->request->isAjax)
            {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['status' => 200];
            }

            return $this->redirect(['view', 'id'=>$model->name]);
        }

        return $this->renderIsAjax('@backend/views/UserManagement/role/update', compact('model'));
    }

    public function actionDelete($id)
    {
        $this->logParam1 = $id;
        parent::actionDelete($id);
    }
}
