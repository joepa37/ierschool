<?php

namespace backend\controllers\UserManagement;

use Yii;

class UserActivityLogController extends \common\components\BaseCrudController
{
    /**
    * @var UserActivityLog
    */
    public $modelClass = 'common\models\UserActivityLog';
    
    public $modelSearchClass = 'backend\models\search\UserActivityLogSearch';
    
    public $enableOnlyActions = ['index', 'view', 'grid-page-size'];

    public $directory = '@backend/views/UserManagement/user-activity-log/';
}
