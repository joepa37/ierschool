<?php

namespace backend\controllers\UserManagement;


use Yii;
use webvimark\modules\UserManagement\controllers\UserVisitLogController as BaseUserVisitLogController;

class UserVisitLogController extends BaseUserVisitLogController
{
    use \common\components\log\BaseTraitController;

    public function actionIndex()
    {
        $searchModel  = $this->modelSearchClass ? new $this->modelSearchClass : null;

        if ( $searchModel )
        {
            $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        }
        else
        {
            $modelClass = $this->modelClass;
            $dataProvider = new ActiveDataProvider([
                'query' => $modelClass::find(),
            ]);
        }
        return $this->renderIsAjax('@backend/views/UserManagement/user-visit-log/index', compact('dataProvider', 'searchModel'));
    }

    public function actionView($id)
    {
        $this->logParam1 = $id;
        return parent::actionView($id);
    }
}
