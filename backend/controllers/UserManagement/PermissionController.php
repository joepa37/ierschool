<?php

namespace backend\controllers\UserManagement;

use webvimark\modules\UserManagement\controllers\PermissionController as BasePermissionController;

use webvimark\modules\UserManagement\components\AuthHelper;
use webvimark\modules\UserManagement\models\rbacDB\AbstractItem;
use webvimark\modules\UserManagement\models\rbacDB\Permission;
use webvimark\modules\UserManagement\models\rbacDB\Route;
use yii\web\Response;
use Yii;

class PermissionController extends BasePermissionController
{
    use \common\components\log\BaseTraitController;

    public function actionIndex()
    {
        $searchModel  = $this->modelSearchClass ? new $this->modelSearchClass : null;

        if ( $searchModel )
        {
            $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        }
        else
        {
            $modelClass = $this->modelClass;
            $dataProvider = new ActiveDataProvider([
                'query' => $modelClass::find(),
            ]);
        }
        return $this->renderIsAjax('@backend/views/UserManagement/permission/index', compact('dataProvider', 'searchModel'));
    }

    public function actionView($id)
    {
        $this->logParam1 = $id;
        $item = $this->findModel($id);

        $routes = Route::find()->asArray()->all();

        $permissions = Permission::find()
            ->andWhere(['not in', Yii::$app->getModule('user-management')->auth_item_table . '.name', [Yii::$app->getModule('user-management')->commonPermissionName, $id]])
            ->joinWith('group')
            ->all();

        $permissionsByGroup = [];
        foreach ($permissions as $permission)
        {
            $permissionsByGroup[@$permission->group->name][] = $permission;
        }

        $childRoutes = AuthHelper::getChildrenByType($item->name, AbstractItem::TYPE_ROUTE);
        $childPermissions = AuthHelper::getChildrenByType($item->name, AbstractItem::TYPE_PERMISSION);

        return $this->renderIsAjax('@backend/views/UserManagement/permission/view', compact('item', 'childPermissions', 'routes', 'permissionsByGroup', 'childRoutes'));
    }
    
    public function actionSetChildPermissions($id)
    {
        $this->logParam1 = $id;
        $this->logParam2 = implode(';', Yii::$app->request->post('child_permissions', []));
        return parent::actionSetChildPermissions($id);
    }
    
    public function actionSetChildRoutes($id)
    {
        $this->logParam1 = $id;
        $this->logParam2 = implode(';', Yii::$app->request->post('child_routes', []));
        return parent::actionSetChildRoutes($id);
    }

    public function actionCreate()
    {
        $model = new Permission();
        $model->scenario = 'webInput';

        if ( $model->load(Yii::$app->request->post()) && $model->save() )
        {
            return $this->redirect(['view', 'id'=>$model->name]);
        }

        return $this->renderIsAjax('@backend/views/UserManagement/permission/create', compact('model'));
    }

    public function actionUpdate($id)
    {
        $this->logParam1 = $id;

        $model = $this->findModel($id);
        $model->scenario = 'webInput';

        if ( $model->load(Yii::$app->request->post()) AND $model->save())
        {
            if(Yii::$app->request->isAjax)
            {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['status' => 200];
            }

            return $this->redirect(['view', 'id'=>$model->name]);
        }

        return $this->renderIsAjax('@backend/views/UserManagement/permission/update', compact('model'));
    }

    public function actionDelete($id)
    {
        $this->logParam1 = $id;
        parent::actionDelete($id);
    }
}
