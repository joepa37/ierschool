<?php

namespace backend\controllers;

use webvimark\modules\UserManagement\controllers\AuthController as BaseAuthController;
use webvimark\modules\UserManagement\components\UserAuthEvent;
use backend\models\forms\ChangeOwnPasswordForm;
use backend\models\forms\LoginForm;
use backend\models\forms\PasswordRecoveryForm;
use backend\models\User;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;

class AuthController extends BaseAuthController
{
    use \common\components\log\BaseTraitController;

    /**
    * @var array
    */
    public $freeAccessActions = ['login', 'logout', 'password-recovery','password-recovery-receive'];
    
    /**
    * @return array
    */
    public function actions()
    {
        return [
            'captcha' => Yii::$app->getModule('user-management')->captchaOptions,
            'avatar-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'avatar-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(215, 215);
                    $file->put($img->encode());
                }
            ],
            'avatar-delete' => [
                'class' => DeleteAction::className()
            ]
        ];
    }
   
    /**
    * Login form
    *
    * @return string
    */
    public function actionLogin()
    {
        $this->layout='base';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ( Yii::$app->request->isAjax AND $model->load(Yii::$app->request->post()) ) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            if($model->login()){
                $this->logParam1 = 'granted';
                return $this->goBack();
            }
            $this->logParam1 = $model->username;
            $this->logParam2 = 'denied';
        }
        return $this->renderIsAjax('@backend/views/auth/login', compact('model'));
    }

    /**
     * Change your own password
     *
     * @throws \yii\web\ForbiddenHttpException
     * @return string|\yii\web\Response
     */    
    public function actionChangePassword()
    {
        if ( Yii::$app->user->isGuest )
        {
            return $this->goHome();
        }
        $user = User::getCurrentUser();
        if ( $user->status != User::STATUS_ACTIVE)
        {
            throw new ForbiddenHttpException('Su usuario no tiene acceso a esta sección.');
        }
        $model = new ChangeOwnPasswordForm(['user'=>$user]);
        if ( Yii::$app->request->isAjax AND $model->load(Yii::$app->request->post()) )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ( $model->load(Yii::$app->request->post()) AND $model->changePassword() )
        {
            $this->logParam1 = 'updated';
            return $this->renderIsAjax('changeOwnPasswordSuccess');
        }
        return $this->renderIsAjax('changeOwnPassword', compact('model'));
    }

    /**
    * Form to recover password
    *
    * @return string|\yii\web\Response
    */    
    public function actionPasswordRecovery()
    {
        $this->layout='base';
        if ( !Yii::$app->user->isGuest )
        {
            return $this->goHome();
        }

        $model = new PasswordRecoveryForm();

        if ( Yii::$app->request->isAjax AND $model->load(Yii::$app->request->post()) )
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            // Ajax validation breaks captcha. See https://github.com/yiisoft/yii2/issues/6115
            // Thanks to TomskDiver
            $validateAttributes = $model->attributes;
            unset($validateAttributes['captcha']);

            return ActiveForm::validate($model, $validateAttributes);
        }

        if ( $model->load(Yii::$app->request->post()) AND $model->validate() )
        {
            if ( $this->triggerModuleEvent(UserAuthEvent::BEFORE_PASSWORD_RECOVERY_REQUEST, ['model'=>$model]) )
            {
                if ( $model->sendEmail(false) )
                {
                    if ( $this->triggerModuleEvent(UserAuthEvent::AFTER_PASSWORD_RECOVERY_REQUEST, ['model'=>$model]) )
                    {
                        $this->logParam1 = 'sent';
                        $this->logParam2 = $model->email;
                        return $this->renderIsAjax('@backend/views/auth/passwordRecoverySuccess');
                    }
                }
                else
                {
                    $this->logParam1 = 'failed';
                    Yii::$app->session->setFlash('error', UserManagementModule::t('front', "Unable to send message for email provided"));
                }
            }
        }
        return $this->renderIsAjax('@backend/views/auth/passwordRecovery', compact('model'));
    }

    /**
     * Receive token, find user by it and show form to change password
     *
     * @param string $token
     *
     * @throws \yii\web\NotFoundHttpException
     * @return string|\yii\web\Response
     */    
    public function actionPasswordRecoveryReceive($token)
    {
        if ( !Yii::$app->user->isGuest )
        {
            return $this->goHome();
        }

        $user = User::findByConfirmationToken($token);

        if ( !$user )
        {
            $this->logParam1 = 'tokenNotFound';
            throw new NotFoundHttpException(UserManagementModule::t('front', 'Token not found. It may be expired. Try reset password once more'));
        }

        $model = new ChangeOwnPasswordForm([
                'scenario'=>'restoreViaEmail',
                'user'=>$user,
        ]);

        if ( $model->load(Yii::$app->request->post()) AND $model->validate() )
        {
            if ( $this->triggerModuleEvent(UserAuthEvent::BEFORE_PASSWORD_RECOVERY_COMPLETE, ['model'=>$model]) )
            {
                $model->changePassword(false);

                if ( $this->triggerModuleEvent(UserAuthEvent::AFTER_PASSWORD_RECOVERY_COMPLETE, ['model'=>$model]) )
                {
                    $this->logParam1 = 'recovered';
                    return $this->renderIsAjax('changeOwnPasswordSuccess');
                }
            }
        }

        return $this->renderIsAjax('changeOwnPassword', compact('model'));
    }
}
