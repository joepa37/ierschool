<?php
namespace backend\models\forms;

use webvimark\modules\UserManagement\models\forms\PasswordRecoveryForm as BasePasswordRecoveryForm;
use webvimark\modules\UserManagement\UserManagementModule;
use backend\models\User;
use Yii;

class PasswordRecoveryForm extends BasePasswordRecoveryForm
{
    /**
     * @var User
     */
    protected $user;

    public function rules()
    {
        return [
                ['captcha', 'captcha', 'captchaAction'=>'/auth/captcha'],

                [['email', 'captcha'], 'required'],
                ['email', 'trim'],
                ['email', 'email'],

                ['email', 'validateEmailConfirmedAndUserActive'],
        ];
    }
    
    /**
     * @param bool $performValidation
     *
     * @return bool
     */
    public function sendEmail($performValidation = true)
    {
        if ( $performValidation AND !$this->validate() )
        {
                return false;
        }

        $this->user->generateConfirmationToken();
        $this->user->save(false);

        try {
            return Yii::$app->mailer->compose(Yii::$app->getModule('user-management')->mailerOptions['passwordRecoveryFormViewFile'], ['user' => $this->user])
                ->setFrom(Yii::$app->getModule('user-management')->mailerOptions['from'])
                ->setTo($this->email)
                ->setSubject(UserManagementModule::t('front', 'Password reset for') . ' ' . Yii::$app->name)
                ->send();            
        } catch (\Swift_TransportException $e){
            return false;
        }
    }
}
