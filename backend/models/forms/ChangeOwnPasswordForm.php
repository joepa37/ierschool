<?php
namespace backend\models\forms;

use webvimark\modules\UserManagement\models\forms\ChangeOwnPasswordForm as BaseChangeOwnPasswordForm;

class ChangeOwnPasswordForm extends BaseChangeOwnPasswordForm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
            return [
                    [['password', 'repeat_password'], 'required'],
                    [['password', 'repeat_password', 'current_password'], 'string', 'max'=>255],
                    [['password', 'repeat_password', 'current_password'], 'trim'],

                    ['password','match','pattern' => '/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d\W]){1,})(?!.*\s).{8,15}$/', 'message' => 'La contraseña debe tener al menos una mayúscula, símbolo o un número con una longitud min de 8 y max 15 caracteres.'],
                    ['repeat_password', 'compare', 'compareAttribute'=>'password'],

                    ['current_password', 'required', 'except'=>'restoreViaEmail'],
                    ['current_password', 'validateCurrentPassword', 'except'=>'restoreViaEmail'],
            ];
    }

    public function attributeLabels()
    {
            return [
                    'current_password' => 'Contraseña Actual',
                    'password'         => 'Nueva Contraseña',
                    'repeat_password'  => 'Confirmar Contraseña',
            ];
    }
}
