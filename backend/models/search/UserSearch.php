<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\User;

/**
 * UserSearch represents the model behind the search form about `backend\models\User`.
 */
class UserSearch extends User
{
	public $fullName;
	public $storeId;
	public function rules()
	{
		return [
			[['id', 'superadmin', 'status', 'storeId'], 'integer'],
			[['fullName','username', 'gridRoleSearch'], 'string'],
		];
	}

	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = User::find();

		$query->with(['roles']);

		if ( !Yii::$app->user->isSuperadmin )
		{
			$query->where(['superadmin'=>0]);
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => Yii::$app->request->cookies->getValue('_grid_page_size', 20),
			],
			'sort'=>[
				'defaultOrder'=>[
					'id'=>SORT_DESC,
				],
			],
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->joinWith(['userProfile']);

		if ( $this->gridRoleSearch )
		{
			$query->joinWith(['roles']);
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'superadmin' => $this->superadmin,
			'status' => $this->status,
			Yii::$app->getModule('user-management')->auth_item_table . '.name' => $this->gridRoleSearch,
		]);

        	$query->andFilterWhere(['like', 'username', $this->username]);
        	$query->andFilterWhere(['like', 'concat(userProfile.firstName,\' \',userProfile.lastName)', $this->fullName]);

		return $dataProvider;
	}
}
