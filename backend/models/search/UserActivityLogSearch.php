<?php

namespace backend\models\search;

use common\models\UserActivityLog;
use yii\data\ActiveDataProvider;
use Yii;
use yii\base\Model;
use backend\models\User;

class UserActivityLogSearch extends UserActivityLog
{
    public function rules()
    {
        return [
            [['user_id', 'controller', 'action', 'param1','os', 'ipaddress', 'logtime'], 'safe'],
        ];
    }
    
    public function scenarios()
    {
        return Model::scenarios();
    }
    
    public function search($params)
    {
        $query = UserActivityLog::find();
        $query->joinWith(['user']);
        
        // Don't let non-superadmin view superadmin activity
        if ( !Yii::$app->user->isSuperadmin )
        {
            $query->andWhere([User::tableName() . '.superadmin'=>0]);
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                    'pageSize' => Yii::$app->request->cookies->getValue('_grid_page_size', 20),
            ],
            'sort'=>[
                    'defaultOrder'=>['id'=> SORT_DESC],
            ],
        ]);
        
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        if(isset($this->user_id) && strtolower($this->user_id) == 'guest'){
            $query->andFilterWhere([static::tableName() . '.user_id' => 0]);
        }else{
            $query->andFilterWhere(['like', User::tableName() . '.username', $this->user_id]);
        }
        
        $query->andFilterWhere(['like', static::tableName() . '.controller', $this->controller])
                ->andFilterWhere(['like', static::tableName() . '.action', $this->action])
                ->andFilterWhere(['like', static::tableName() . '.os', $this->os])
                ->andFilterWhere(['like', static::tableName() . '.browser', $this->browser])
                ->andFilterWhere(['like', static::tableName() . '.ipaddress', $this->ipaddress]);        
        
        if ( $this->logtime )
        {
            $tmp = explode(' - ', $this->logtime);
            if ( isset($tmp[0], $tmp[1]) )
            {
                $query->andFilterWhere(['between', static::tableName() . '.logtime', $tmp[0], $tmp[1]]);
            }
        }        
        
        return $dataProvider;
    }
}
