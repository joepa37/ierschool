<?php

namespace backend\models;

use webvimark\modules\UserManagement\models\User as BaseUser;
use common\models\UserProfile;


/**
 * User model
 *
 *
 * @property \common\models\UserProfile $userProfile
 */

class User extends BaseUser
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getPublicIdentity()
    {
        if ($this->userProfile && $this->userProfile->getFullname()) {
            return $this->userProfile->getFullname();
        }
        if ($this->username) {
            return $this->username;
        }
        return $this->email;
    }
}
