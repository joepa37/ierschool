<?php

use webvimark\modules\UserManagement\UserManagementModule;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var webvimark\modules\UserManagement\models\forms\PasswordRecoveryForm $model
 */

$this->title = UserManagementModule::t('front', 'Password recovery');
$this->params['breadcrumbs'][] = $this->title;
$this->params['body-class'] = 'login-page';
?>

	<div class="login-box">
		<div class="login-logo">
			<?php echo Html::encode($this->title) ?>
		</div><!-- /.login-logo -->
		<div class="header"></div>
		<div class="login-box-body">
			<?php if(Yii::$app->session->hasFlash('error')):
				echo Alert::widget([
					'options' => [
						'class' => 'alert-warning',
					],
					'body' => Yii::$app->session->getFlash('error'),
				]);
			endif; ?>

			<?php $form = ActiveForm::begin([
				'id'=>'user',
			]); ?>

			<div class="body">

				<?= $form->field($model, 'email')->textInput(['maxlength' => 255, 'autofocus'=>true]) ?>

				<?= $form->field($model, 'captcha')->widget(Captcha::className(), [
					'template' => '<div class="row"><div class="col-sm-4">{image}</div><div class="col-sm-8">{input}</div></div>',
					'captchaAction'=>['/auth/captcha']
				]) ?>
			</div>
			<div class="footer">
				<?= Html::submitButton(
					'<span class="glyphicon glyphicon-ok"></span> ' . UserManagementModule::t('front', 'Recover'),
					['class' => 'btn btn-lg btn-primary btn-block']
				) ?>
			</div>
					<?php ActiveForm::end(); ?>
		</div>

	</div>