<?php
use webvimark\modules\UserManagement\components\GhostHtml;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\LoginForm */

$this->title = Yii::t('backend', 'Sign In');
$this->params['breadcrumbs'][] = $this->title;
$this->params['body-class'] = 'login-page';
?>
<div class="login-box">
	<div class="login-logo">
		<?php echo Html::encode($this->title) ?>
	</div>
	<div class="header"></div>
	<div class="login-box-body">
		<?php $form = ActiveForm::begin(['id' => 'login-form', 'options'=>['autocomplete'=>'off']]); ?>
		<div class="body">
			<?= $form->field($model, 'username')
				->textInput(['placeholder'=>$model->getAttributeLabel('username'), 'autocomplete'=>'off', 'autofocus' => true,]) ?>

			<?= $form->field($model, 'password')
				->passwordInput(['placeholder'=>$model->getAttributeLabel('password'), 'autocomplete'=>'off']) ?>

			<?= $form->field($model, 'rememberMe')->checkbox(['value'=>true]) ?>
		</div>
		<div class="footer">
			<?= Html::submitButton(
				'<span class="glyphicon glyphicon-ok"></span> ' . Yii::t('backend', 'Sign me in'),
				['class' => 'btn btn-lg btn-primary btn-block'],
				['name' => 'login-button']
			) ?>

			<div class="row">
				<div class="col-sm-12 text-right">
					<?php echo Html::a(UserManagementModule::t('front', "Forgot password ?"), ['/auth/password-recovery']) ?>
				</div>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>