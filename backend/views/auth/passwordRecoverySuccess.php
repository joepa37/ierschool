<?php

use webvimark\modules\UserManagement\UserManagementModule;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 */

$this->title = UserManagementModule::t('front', 'Password recovery');
$this->params['breadcrumbs'][] = $this->title;
$this->params['body-class'] = 'login-page';
?>
<div class="login-box">
	<div class="login-logo">
		<?php echo Html::encode($this->title) ?>
	</div>
	<div class="body">
		<div class="alert alert-success text-center">
			<?= UserManagementModule::t('front', 'Check your E-mail for further instructions') ?>
		</div>
	</div>
</div>
