<?php
/* @var $this yii\web\View */

use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log de Actividad', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-activity-log-view">

    <div class="panel panel-default">
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute'=>'user_id',
                        'value'=>isset($model->user) ? @$model->user->username : 'Guest',
                    ],
                    'controller',
                    'action',
                    'param1',
                    'param2',
                    'param3',
                    'param4',
                    'extra_params',
                    'os',
                    'browser',
                    'browser_version',
                    'user_agent',
                    'ipaddress',
                    'logtime:datetime',
                ],
            ]) ?>
        </div>
    </div>
</div>