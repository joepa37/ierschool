<?php
/**
 * @var $this yii\web\View
 * @var $user webvimark\modules\UserManagement\models\User
 */
use yii\helpers\Html;

?>
<p>Buen día,
<br><br>
La presente es para informarle que se le creo su usuario para ingresar al sistema de IER School.
<br><br>
URL: http://ierschool.dev/<br>
Usuario: <?= Html::encode($user->username) ?><br>
Contraseña: <?= $pwd ?>
<br><br>
Saludos</p>