<?php
$config = [
    'homeUrl'=>Yii::getAlias('@backendUrl'),
    'controllerNamespace' => 'backend\controllers',
    'defaultRoute'=>'timeline-event/index',
    'controllerMap'=>[
        'file-manager-elfinder' => [
            'class' => 'mihaildev\elfinder\Controller',
            //'access' => ['manager'],
            'disabledCommands' => ['netmount'],
            'roots' => [
                [
                    'baseUrl' => '@storageUrl',
                    'basePath' => '@storage',
                    'path'   => '/',
                    //'access' => ['read' => 'manager', 'write' => 'manager']
                ]
            ]
        ]
    ],
    'components'=>[
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' => [
            'cookieValidationKey' => env('BACKEND_COOKIE_VALIDATION_KEY')
        ],
        'user' => [
            'class' => 'common\components\UserConfig',
            'identityClass' => 'backend\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['auth/login'],
            'on afterLogin' => function($event) {
                \webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
            }
        ],
    ],
    'modules'=>[
        'i18n' => [
            'class' => 'backend\modules\i18n\Module',
            'defaultRoute'=>'i18n-message/index'
        ],
        'user-management' => [
            'class' => 'webvimark\modules\UserManagement\UserManagementModule',
            'controllerNamespace' => 'backend\controllers\UserManagement',
            'emailConfirmationRequired' => false,
            'useEmailAsLogin' => true,
            'maxAttempts' => 4,
            'enableRegistration' => false,
            'confirmationTokenExpire' => 28800, //8 hours
            'mailerOptions' => [
                'from' => [
                    'no_reply@ierschool.dev' => 'IER School'
                ],
                'passwordRecoveryFormViewFile' => '@backend/mail/UserManagement/PasswordRecoveryMail',
            ],
        ],
    ],
];

if (YII_ENV_DEV) {
    $config['modules']['gii'] = [
        'class'=>'yii\gii\Module',
        'generators' => [
            'crud' => [
                'class'=>'yii\gii\generators\crud\Generator',
                'templates'=>[
                    'ierschool' => Yii::getAlias('@backend/views/_gii/templates')
                ],
                'template' => 'ierschool',
                'messageCategory' => 'backend'
            ]
        ]
    ];
}

return $config;
