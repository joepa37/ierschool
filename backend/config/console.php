<?php
return [
    'id' => 'backend-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'modules' => [
        'gii' => 'yii\gii\Module',
        'user-management' => [
            'class' => 'webvimark\modules\UserManagement\UserManagementModule',
        ],
    ],
];
